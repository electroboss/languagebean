from index.models import *
from django.contrib.auth.models import Group, User, Permission
import datetime
multiple_choice = Game(game_name="Multiple choice", game_contents="""<h1 id="original_word">Original word</h1>
<div id="score">Score: 0</div>
<div id="center-container">
    <div id="container">
        <button class="option" onclick="choose(0);" id="button0">Translated word</button>
        <button class="option" onclick="choose(1);" id="button1">Translated word</button>
        <button class="option" onclick="choose(2);" id="button2">Translated word</button>
        <button class="option" onclick="choose(3);" id="button3">Translated word</button>
    </div>
    <div id="win" style="display:none;">You won!</div>
</div>""")

multiple_choice.save()

teachers = Group.objects.create(name="teachers")
teachers.permissions.add(Permission.objects.get(codename="add_assignmentlist"))
teachers.save()

new_user = User.objects.create_user('test1','test1@example.com','debug')
new_user.save()
new_user2 = User.objects.create_user('test2','test2@example.com','deerror')
new_user2.first_name = "Test"
new_user2.last_name = "Testson"
new_user2.groups.add(teachers)
new_user2.save()

new_user_group = UserGroup(name="testClass",teacher=new_user2)
new_user_group.save()
new_user_student_group = StudentOfUserGroup(user=new_user,user_group=new_user_group)
new_user_student_group.save()

colours = WordList(name="colours", original_language="English", translated_language="French")
colours.save()
orig = ["red","blue","green","yellow","orange","black","white","grey","purple","pink"]
tran = ["rouge", "bleu", "vert", "jaune", "orange", "noir", "blanc", "gris", "violet", "rose"]
for x in range(10):
  w = Word(word_list=colours, original_word=orig[x], translated_word=tran[x])
  w.save()

new_assignment = AssignmentList(due_date=datetime.date(2021, 6, 9), set_by=new_user2, set_to=new_user_group)
new_assignment.save()
assignment = Assignment(word_list=colours, assignment_list=new_assignment, game=multiple_choice)
assignment.save()

