from django.db import models
from django.contrib.auth.models import User

class Game(models.Model):
    game_name = models.CharField(max_length=20, default="Multiple Choice")
    game_contents = models.CharField(max_length=1000, default="""
<h1 id="original_word">Original word</h1>
<div id="score">Score: 0</div>
<div id="center-container">
    <div id="container">
        <button class="option" onclick="choose(0);" id="button0">Translated word</button>
        <button class="option" onclick="choose(1);" id="button1">Translated word</button>
        <button class="option" onclick="choose(2);" id="button2">Translated word</button>
        <button class="option" onclick="choose(3);" id="button3">Translated word</button>
    </div>
    <div id="win" style="display:none;">You won!</div>
</div>""")

class WordList(models.Model):
    name = models.CharField(max_length=20, default="Word List")
    original_language = models.CharField(max_length=20)
    translated_language = models.CharField(max_length=20)

class Word(models.Model):
    word_list = models.ForeignKey(WordList, on_delete=models.CASCADE)
    original_word = models.CharField(max_length=20)
    translated_word = models.CharField(max_length=20)

    def __str__(self):
        return self.word_list.original_language+": "+self.original_word+", "+self.word_list.translated_language+": "+self.translated_word

class UserGroup(models.Model):
    name = models.CharField(max_length=20)
    teacher = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.name + ", Teacher: " + self.teacher.username

class StudentOfUserGroup(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    user_group = models.ForeignKey(UserGroup, on_delete=models.CASCADE)

class AssignmentList(models.Model):
    set_by = models.ForeignKey(User, on_delete=models.CASCADE)
    set_to = models.ForeignKey(UserGroup, on_delete=models.CASCADE)
    due_date = models.DateField("Date due")

class Assignment(models.Model):
    word_list = models.ForeignKey(WordList, on_delete=models.CASCADE)
    assignment_list = models.ForeignKey(AssignmentList, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)