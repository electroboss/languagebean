var wordIndex = 0
var score = 0
var buttons = [];

function choose(id) {
    if (id == buttons.indexOf(wordIndex)){
        score++;
    } else {
        score--;
    }
    updateScore()
    next()
}

function next() {
    wordIndex++;
    if (wordIndex >= 10) {
        completed()
    } else {
        document.getElementById("original_word").innerHTML = original_words[wordIndex];

        buttons = []
        while(buttons.length < 4){
            var r = Math.floor(Math.random() * 10);
            if(buttons.indexOf(r) === -1) buttons.push(r);
        }

        if (!buttons.includes(wordIndex)){
            buttons[Math.floor(Math.random()*4)] = wordIndex
        }
        document.getElementById("button0").innerHTML = translated_words[buttons[0]]
        document.getElementById("button1").innerHTML = translated_words[buttons[1]]
        document.getElementById("button2").innerHTML = translated_words[buttons[2]]
        document.getElementById("button3").innerHTML = translated_words[buttons[3]]
    }
}
next()
function completed() {
    document.getElementById("win").style.display = "block";
    document.getElementById("container").style.display = "none";
    document.getElementById("original_word").style.display = "none";
    document.getElementById("win").innerHTML = "You've completed all 10 words. Score: "+score.toString()+"<br /><a href=\"/\">Back</a>";
    document.getElementById("center-container").style.marginTop = "25%";
}
function updateScore(){
    document.getElementById("score").innerHTML = "Score: "+score.toString()
}