from django.urls import path

from . import views

app_name = "index"
urlpatterns = [
    path("", views.HomePage, name="HomePage"),
    path('assignments/', views.GameList, name='GameList'),
    path('game/<int:game_id>/<int:word_list_id>/', views.GamePage, name='GamePage'),
    path('assignments/lists/add', views.AssignmentCreationPage, name='AssignmentCreationPage'),
    path('assignments/done/', views.CreateAssignment, name='CreateAssignment'),
    path("assignments/list/<int:assignment_list_id>/add", views.AddAssignments, name="AddAssignments"),
    path("assignments/list/<int:assignment_list_id>/adding", views.AddAssignmentToSQL, name="AddAssignmentToSQL"),
    path("assignments/list/<int:assignment_list_id>/remove/<int:assignment_id>/", views.RemoveAssignmentSQL, name="RemoveAssignmentSQL"),
    path("assignments/lists/", views.AssignmentLists, name="AssignmentLists"),
    path("assignments/list/<int:assignment_list_id>/remove/", views.AssignmentListRemove, name="AssignmentListRemove"),
]
