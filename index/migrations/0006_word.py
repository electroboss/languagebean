# Generated by Django 3.2 on 2021-05-09 19:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0005_delete_word'),
    ]

    operations = [
        migrations.CreateModel(
            name='Word',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('original_word', models.CharField(max_length=20)),
                ('translated_word', models.CharField(max_length=20)),
                ('word_list', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='index.wordlist')),
            ],
        ),
    ]
