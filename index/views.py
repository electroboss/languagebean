from django.shortcuts import render
from django.http import Http404, HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.templatetags.static import static
from django.urls import reverse
from django.contrib.auth import authenticate, login, logout

from .models import *

def GameList(request):
    if request.user.is_authenticated:
        assignment_lists = AssignmentList.objects.filter(set_to__studentofusergroup__user=request.user)
        return render(request, "index/game_list.html", {"assignment_lists":assignment_lists})
    else:
        return HttpResponseForbidden("You need to be logged in to access this page")

def GamePage(request, game_id, word_list_id):
    word_list = WordList.objects.get(pk=word_list_id)
    game = Game.objects.get(pk=game_id)
    stylesheet = static("index/game"+str(game.pk)+".css")
    script = static("index/game"+str(game.pk)+".js")
    return render(request, "index/game.html", {"word_list":word_list, "game":game, "script":script, "stylesheet":stylesheet})

def HomePage(request):
    return render(request, "index/index.html", {"login":1 if request.user.is_authenticated else 0, "teacher":request.user.has_perm("index.add_assignmentlist"), "student":(not request.user.has_perm("index.add_assignmentlist")) and request.user.is_authenticated})

def AssignmentCreationPage(request):
    if request.user.is_authenticated and request.user.has_perm("index.add_assignmentlist"):
        classes = UserGroup.objects.filter(teacher=request.user)
        return render(request, "index/create_assignment.html", {"classes":classes})
    else:
        return HttpResponseForbidden("You need to be logged in and a teacher to access this page")

def CreateAssignment(request):
    assignment = AssignmentList(set_by=request.user, set_to=UserGroup.objects.get(pk=request.POST['class']), due_date=request.POST["due"])
    assignment.save()
    return HttpResponseRedirect(reverse("index:AddAssignments",kwargs={"assignment_list_id":assignment.pk}))

def AddAssignments(request, assignment_list_id):
    try:
        assignmentList = AssignmentList.objects.get(pk=assignment_list_id)
    except:
        return HttpResponseForbidden("This assignment list does not exist")
    if not request.user.is_authenticated or not request.user.has_perm("index.add_assignmentlist"):
        return HttpResponseForbidden("You need to be logged in and a teacher to access this page")
    if assignmentList.set_by != request.user:
        return HttpResponseForbidden("You have not made this assignment list.")
    Games = Game.objects.all()
    WordLists = WordList.objects.all()
    return render(request, "index/add_assignments.html", {"list": assignmentList, "Games":Games, "WordLists":WordLists})
    
def AddAssignmentToSQL(request, assignment_list_id):
    try:
        assignmentList = AssignmentList.objects.get(pk=assignment_list_id)
    except:
        return HttpResponseForbidden("This assignment list does not exist")
    if not request.user.is_authenticated or not request.user.has_perm("index.add_assignmentlist"):
        return HttpResponseForbidden("You need to be logged in and a teacher to access this page")
    if assignmentList.set_by != request.user:
        return HttpResponseForbidden("You have not made this assignment list.")
    
    assignment = Assignment(word_list=WordList.objects.get(pk=request.POST["wordlist"]),
                            assignment_list=assignmentList,
                            game=Game.objects.get(pk=request.POST["game"]))
    assignment.save()
    return HttpResponseRedirect(reverse("index:AddAssignments", kwargs={"assignment_list_id":assignment_list_id}))

def RemoveAssignmentSQL(request, assignment_list_id, assignment_id):
    assignment = Assignment.objects.get(pk=assignment_id)
    if assignment.assignment_list.set_by != request.user:
        return HttpResponseForbidden("You have not made this assignment list.")
    elif assignment.assignment_list != AssignmentList.objects.get(pk=assignment_list_id):
        return HttpResponseForbidden("This assignment is not part of this assignment list.")
    
    assignment.delete()
    return HttpResponseRedirect(reverse("index:AddAssignments", kwargs={"assignment_list_id":assignment_list_id}))

def AssignmentLists(request):
    if request.user.is_authenticated and request.user.has_perm("index.add_assignmentlist"):
        assignment_lists = AssignmentList.objects.filter(set_by=request.user)
        return render(request, "index/assignment_lists.html", {"lists":assignment_lists})
    else:
        return HttpResponseForbidden("You need to be logged in and a teacher to access this page")

def AssignmentListRemove(request, assignment_list_id):
    assignment_list = AssignmentList.objects.get(pk=assignment_list_id)
    if assignment_list.set_by != request.user:
        return HttpResponseForbidden("You have not made this assignment list.")
    
    assignment_list.delete()
    return HttpResponseRedirect(reverse("index:AssignmentLists"))